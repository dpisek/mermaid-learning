# Mermaid Learning

## UML Class Diagram

### Associations

Assotiations are the loosest type of relationship available within a UML class diagram.

The entities must be able to exist independently of one another. There is also no "owner" of the relationship, they are simply linked.

Example:

```mermaid
classDiagram
    Title -- Genre
```

## State Diagram

https://mermaid.js.org/syntax/stateDiagram.html#states

```mermaid
stateDiagram-v2
    [*] --> noob
    noob --> intermediate: learning
    intermediate --> pro: more learning
    pro --> noob : realising that you know nothing
    pro --> enlightenment:go for a nature walk
    %% this is a note
    note right of enlightenment
        becoming one with mermaid
    end note
    state if_state <<choice>>
    enlightenment --> if_state
    state comfort: comfort {
        if_state --> low: if temperature < 15°C
        note right of low
            enlightened, but still human!
        end note
        if_state --> high : if temperature >= 15°C
    }
```

### JS Promise

```mermaid
stateDiagram-v2
    [*] --> Pending: New Promise()
    Pending --> Fulfilled: resolve()
    Pending --> Rejected: reject()
    Fulfilled --> [*]: Promise settled
    Rejected --> [*]: Promise settled
```

### Street light

```mermaid
stateDiagram-v2
    [*] --> Red: Initialize
    Red --> Yellow: Red timer expires
    Yellow --> Green: Yellow timer expires
    Green --> Yellow: Green timer expires
    Yellow --> Red: Yellow timer expires (2nd time)
```

## User journey

```mermaid
journey
    title My working day
    section Go to work
      Make tea: 5: Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 5: Me
```

## Flow Charts

### Clickable links

```mermaid
    graph LR;
    A-->B;
    click B "http://www.gitlab.com"
```

## Gantt Charts

```mermaid
gantt
    dateFormat  YYYY-MM-DD
    title Adding GANTT diagram functionality to mermaid

    section A section
    Completed task            :done,    des1, 2014-01-06,2014-01-08
    Active task               :active,  des2, 2014-01-09, 3d
    Future task               :         des3, after des2, 5d
    Future task2              :         des4, after des3, 5d
```

### HTTP Request

```mermaid
gantt
    dateFormat  YYYY-MM-DD
    title HTTP Request Process
    excludes weekends

    section Client
    Prepare Request       :done,  des1, 2023-04-24, 1d
    Send Request          :active, des2, 2023-04-25, 1d

    section Server
    Receive Request       :crit,   des3, 2023-04-26, 1d
    Process Request       :crit,   des4, 2023-04-27, 2d
    Send Response         :crit,   des5, 2023-04-29, 1d

    section Client
    Receive Response      :done,   des6, 2023-04-30, 1d
    Process Response      :done,   des7, 2023-05-01, 1d

```

## Mindmaps

```mermaid
mindmap
  root((mindmap))
    Origins
      Long history
      ::icon(fa fa-book)
      Popularisation
        British popular psychology author Tony Buzan
    Research
      On effectiveness<br/>and features
      On Automatic creation
        Uses
            Creative techniques
            Strategic planning
            Argument mapping
    Tools
      Pen and paper
      Mermaid
```
